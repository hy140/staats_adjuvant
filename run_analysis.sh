#!/bin/bash
#SBATCH -p chsi
#SBATCH -N 1
#SBATCH -n 84
#SBATCH --mem-per-cpu=8000

mkdir -p /scratch/staats_adjuvant

# pull the repo and put content in work/ directory
cd /scratch/staats_adjuvant
git clone https://hy140:bey-W2VncNc9XYbwXE7g@gitlab.oit.duke.edu/hy140/staats_adjuvant.git
mv staats_adjuvant work
# download the datasets and move them to the right place
mkdir -p /scratch/staats_adjuvant/data
cd /scratch/staats_adjuvant/data
source /hpc/group/chsi/hy140/ddsclient/bin/activate
ddsclient download -p 'staats_adjuvant' /scratch/staats_adjuvant/data
mv /scratch/staats_adjuvant/data/F19FTSUSAT1079_MUSxkyE/CleanData/* /scratch/staats_adjuvant/data
rm -rf /scratch/staats_adjuvant/data/F19FTSUSAT1079_MUSxkyE/
deactivate
# make the directory that holds intermediate results
mkdir -p /scratch/staats_adjuvant/scratch
# make the directory that holds the final results
mkdir -p /scratch/staats_adjuvant/results
# pull the container image
cd /scratch/staats_adjuvant
curl -O https://research-singularity-registry.oit.duke.edu/hy140/staats_adjuvant.sif
# run the analysis in the container
singularity exec --bind work:/work,data:/data,scratch:/scratch,results:/analysis_results staats_adjuvant.sif /work/merged/run_all.sh
# make directory in /hpc/group/chsi/your_netid and copy results to there
mkdir -p /hpc/group/chsi/$USER/staats_adjuvant
cp -r /scratch/staats_adjuvant/results /hpc/group/chsi/$USER/staats_adjuvant

rm -r /scratch/staats_adjuvant
