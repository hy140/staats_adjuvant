set -u

# Input
export DATA_BASE="/data"
export RAW_FASTQS="$DATA_BASE"

# Output
export CUROUT=/scratch
export RESULT=/analysis_results
export SUBSAMPL=$CUROUT/sub_sampling
export TRIMMED=$CUROUT/trimmed_fastqs
export MYINFO=$CUROUT/myinfo
export GENOME_DIR=$CUROUT/genome
export STAR_OUT=$CUROUT/star_out
export COUNT_OUT=$CUROUT/count_out
export QC_RAW=$RESULT/qc_output_raw
export QC_TRIM=$RESULT/qc_output_trim
export IGV_DIR="$CUROUT/igv"
export ADAPTERS=$MYINFO/TruSeq_and_BGI_adapters.fa
export THREADS=2
export DIFF_EXP_RES=$RESULT/diff_exp_genes
export DIFF_EXP_SCR=$CUROUT/diff_exp_genes
export VISUAL=$RESULT/visualization

# Genome
export FA_URL="ftp://ftp.ensemblgenomes.org/pub/release-39/fungi/fasta/fungi_basidiomycota1_collection/cryptococcus_neoformans_var_grubii_h99/dna/Cryptococcus_neoformans_var_grubii_h99.CNA3.dna.toplevel.fa.gz"
export GTF_URL="ftp://ftp.ensemblgenomes.org/pub/release-39/fungi/gtf/fungi_basidiomycota1_collection/cryptococcus_neoformans_var_grubii_h99/Cryptococcus_neoformans_var_grubii_h99.CNA3.39.gtf.gz"
export GTF=$(basename ${GTF_URL%.gz})
export FA=$(basename ${FA_URL%.gz})
