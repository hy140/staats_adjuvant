# Mast Cell Bulk-sequencing for Vaccine Adjuvant Study (with Herman Staats)

This Gitlab repo contains 
1. the Singularity container recipe (with the container image being created and pushed to Duke OIT Singularity repository) 
2. all notebooks for analysis (split in parts for ease to debug, merged notebooks can be found in merged)
3. revcomp.py script to reverse complement the adapter files (for trimming purposes)
4. run_analysis.sh that automates the whole data processing pipeline on DCC

Raw data stored on DDS (Josh is the administrator) https://dataservice.duke.edu/#/project/50e0f5d0-23f0-44af-8d73-e981dc480d5d
The DDS repo also contains the sequencing report from BGI and metadata sheets

The local version of the files are stored in bubbles
Directory content (directory located on bubbles --- /data/hy140/staats_adjuvant)
1. Singularity container recipe --- Singularity.rcp
2. Singularity container --- container.sif
3. All jupyter notebooks for analysis and plots --- /data/hy140/staats_adjuvant/work
4. All notebooks are also pushed to github --- https://github.com/duke-chsi-informatics/staats_adjuvant
5. All raw data --- /data/hyang/staats_adjuvant/data


To run the analysis on local machine, first do the following four things to set up the environment
1. download raw datasets (from DDS) into a data directory
2. put all the notebooks in this repo into a work directory 
3. use the singularity container recipe Singularity.def to build the container locally with 
```
singularity build --fakeroot container.sif Singularity.def
```
3. create a scratch directory to store all intermediate results
4. create a results directory to hold the final results
5. Run the following command to fire up the container and start the jupyter notebook interface
```
singularity exec --bind /path_to_data:/data,/path_to_work:/work,/path_to_scratch:/scratch,/path_to_results:/analysis_results container.sif jupyter notebook
``` 
Now we are good to go. Simply run the following three notebooks in order 
1. cd to /work/merged 
2. run all steps in Preprocessing.ipynb
3. run all steps in DESeq2_and_Visualizations.ipynb
4. run all steps in Top20_Genes_Selection.ipynb


To run the analysis on DCC, pull the whole repo to DCC and submit run_analsis.sh through
```
sbatch staats_adjuvant/run_analysis.sh
```
The data will be automatically downloaded and run through the whole pipeline. The final results, including pathview graphs, top enhanced/prohibited genes and notebooks with PCA and Venn graphs, will automatically be directed to store in 
```
/hpc/group/chsi/$USER/staats_adjuvant/results
```

The total run time for the whole analysis is 9 hours.


Main Steps in the analysis
0. Mouse genome download and indexing with STAR
1. Adapter sequence preparation (for trimming)
2. Pre-trim QC
3. Adapter and quality trimming
4. Post-trim QC
5. Map reads to reference genome created above in step 0.
6. Create Count Matrix
7. Bar plot for mapping statistics (number of reads mapped, percentage of reads uniquely mapped)
8. Create file for metadata (necessary for DESeq2 and other downstream analysis)
9. Use DESeq2 to get significant genes for all combinations of condition pairs
10. Draw Venn Diagram to visualize number of genes selected as significant for the pairs (Vehicle, MPL) (Vehicle, M7) (Vehicle, adjuvant) and store genes belong to the three-way intersection
11. Based on result of 9, use heatmap to plot the hamming distance between each condition pair (a significantly expressed gene adds 1 to the hamming distance between a pair)
12. Gene Set Enrichment Analysis on KEGG pathways, both up-regulation and down-regulation in comparing (Vehicle, MPL) , (Vehicle, M7) 
13. KEGG pathview for the pairs (adjuvant, Vehicle) for all adjuvant; for each pair, Hermann chose different pathways to look at, the ones chosen are given in the notebook; 
14. Rank genes by adjusted p-values in the differential expression test for pair (MPL, Vehicle) (M7, Vehicle) and also find the ranking of some specifically chosen cytokines (by Herman) 
15. Gene Set Enrichment Analysis on GO pathways, both up-regulation and down-regulation separately for the pair (Vehicle, MPL), (Vehicle, M7)
16. Obtain top 20 most significant genes (up-regulation and down-regulation separately) for each pair (adjuvant, Vehicle);

13 and 16 are highlighted in red since they are the final results provided to Hermann. 



