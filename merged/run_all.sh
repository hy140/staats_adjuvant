jupyter nbconvert --to pdf /work/merged/Preprocessing.ipynb --execute --output-dir /analysis_results
jupyter nbconvert --to pdf /work/merged/DESeq2_and_Visualization.ipynb --execute --output-dir /analysis_results
jupyter nbconvert --to pdf /work/merged/Top20_Genes_Selection.ipynb --execute --output-dir /analysis_results 
